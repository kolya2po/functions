const getSum = (str1, str2) => {
  if (str1 === '' && str2 !== '') {
    return str2;
  }
  if (str1 !== '' && str2 === '') {
    return str1;
  }
  if (isNaN(Number.parseInt(str1)) || isNaN(Number.parseInt(str2))) {
    return false;
  }
  return (Number.parseInt(str1) + Number.parseInt(str2)).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  for (let p of listOfPosts) {
    if (p.author === authorName) {
      posts++;
    }
    if (p.comments !== undefined) {
      for (let comment of p.comments) {
        if (comment.author === authorName) {
          comments++;
        }
      }      
    }
  }
  return 'Post:' + posts + ',comments:' + comments;
};

const tickets=(people)=> {
  let a25 = 0, a50 = 0;
  for (let i = 0;i < people.length; i++) {
    if (people[i] === 25) {
      a25 += 1;
    }
    if (people[i] === 50) {
      a25 -= 1;
      a50 += 1;
    }
    if (people[i] === 100) {
      if(a50 === 0 && a25 >= 3) {
        a25 -= 3;
      } else {
        a25 -= 1; a50 -= 1;
      }
    }
    if (a25 < 0 || a50 < 0) {
      return 'NO';
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
